<?php namespace Oppin\POS\Tests\API\V1;

use PluginTestCase;
use Faker\Factory;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Oppin\POS\Models\Terminal;
use System\Classes\PluginManager;
use Carbon\Carbon;
use Oppin\POS\Models\Product;
use Oppin\POS\Classes\Total;
use Oppin\POS\Classes\Tax;
use Oppin\POS\Models\Payment;
use Oppin\POS\Classes\Change;
use Oppin\POS\Models\TransactionLine;
use Oppin\POS\Models\Transaction;
use Oppin\POSHospitality\Models\AccountAdjustment;
use Oppin\POS\Classes\VoidTransaction;

/**
 * @coversDefaultClass \Oppin\POSHospitality\Plugin
 */
class PluginTest extends PluginTestCase
{
    public function setUp()
    {
        parent::setUp();

        // Get the plugin manager
        $pluginManager = PluginManager::instance();

        // Register the plugins to make features like file configuration available
        $pluginManager->registerAll(true);

        // Boot all the plugins to test with dependencies of this plugin
        $pluginManager->bootAll(true);
    }

    public function tearDown()
    {
        parent::tearDown();

        // Get the plugin manager
        $pluginManager = PluginManager::instance();

        // Ensure that plugins are registered again for the next test
        $pluginManager->unregisterAll();
    }

    public function dataBootProcessTransaction()
    {
        return [
            'Account top up' => [
                [
                    [
                        'lines' => [
                            [
                                'local_id' => 1,
                                'product_id' => '1',
                                'name' => 'Account Payment',
                                'short_name' => 'Account Payment',
                                'qty' => 1,
                                'single_amount' => '98',
                                'line_amount' => '98',
                                'line_tax' => '0',
                                'type' => 'account_payment',
                                'created_at' => '2019-01-23T22:05:26.558Z'
                            ],
                            [
                                'local_id' => 2,
                                'name' => 'Total',
                                'short_name' => 'Total',
                                'qty' => 1,
                                'single_amount' => '98',
                                'line_amount' => '98',
                                'line_tax' => '0',
                                'type' => 'total',
                                'created_at' => '2019-01-23T22:05:36.991Z'
                            ],
                            [
                                'local_id' => 3,
                                'name' => 'VAT',
                                'short_name' => 'VAT',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'tax',
                                'created_at' => '2019-01-23T22:05:36.992Z'
                            ],
                            [
                                'local_id' => 4,
                                'payment_id' => 1,
                                'name' => 'Cash',
                                'short_name' => 'Cash',
                                'qty' => -1,
                                'single_amount' => '98',
                                'line_amount' => '-98',
                                'line_tax' => '0',
                                'type' => 'payment',
                                'created_at' => '2019-01-23T22:05:36.995Z'
                            ],
                            [
                                'local_id' => 5,
                                'name' => 'Change',
                                'short_name' => 'Change',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'change',
                                'created_at' => '2019-01-23T22:05:36.998Z'
                            ]
                        ],
                        'account_id'       => 1,
                        'delivery_type_id' => 2,
                        'local_id'         => 197,
                        'status'           => 'complete',
                        'currency'         => 'GBP',
                        'user_id'          => 1,
                        'created_at'       => '2019-01-23T22:05:15.430Z',
                    ]
                ],
                [
                    [
                        'account_id'       => 1,
                        'delivery_type_id' => 2,
                        'client_id'        => 197,
                        'parent_id'        => null,
                        'terminal_id'      => 1,
                        'user_id'          => 1,
                        'currency'         => 'GBP',
                        'status'           => 'complete',
                        'created_at'       => new Carbon('2019-01-23T22:05:15.430Z'),
                    ]
                ],
                [
                    [
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => AccountAdjustment::class,
                            'name'           => 'Account Payment',
                            'short_name'     => 'Account Payment',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 98,
                            'line_amount'    => 98,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T22:05:26.558Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Total::class,
                            'name'           => 'Total',
                            'short_name'     => 'Total',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 98,
                            'line_amount'    => 98,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T22:05:36.991Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Tax::class,
                            'name'           => 'VAT',
                            'short_name'     => 'VAT',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T22:05:36.992Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => Payment::class,
                            'name'           => 'Cash',
                            'short_name'     => 'Cash',
                            'tax_rate'       => null,
                            'qty'            => -1,
                            'single_amount'  => 98,
                            'line_amount'    => -98,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T22:05:36.995Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Change::class,
                            'name'           => 'Change',
                            'short_name'     => 'Change',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T22:05:36.998Z')
                        ]
                    ]
                ],
                [
                    [
                        [
                            'id'              => 1,
                            'payment_type_id' => 1,
                            'amount'          => 98
                        ]
                    ]
                ],
                [
                    [
                        'id'              => 1,
                        'current_balance' => 98,
                    ]
                ],
                [
                    [
                        [
                            'id'              => 1,
                            'account_id'      => 1,
                            'amount'          => 98,
                            'description'     => 'POS Deposit',
                            'created_at'      => new Carbon('2019-01-23T22:05:26.558Z')
                        ]
                    ]
                ]
            ],
            'Account top up then void' => [
                [
                    [
                        'lines' => [
                            [
                                'local_id' => 1,
                                'product_id' => '1',
                                'name' => 'Account Payment',
                                'short_name' => 'Account Payment',
                                'qty' => 1,
                                'single_amount' => '98',
                                'line_amount' => '98',
                                'line_tax' => '0',
                                'type' => 'account_payment',
                                'created_at' => '2019-01-23T22:05:26.558Z'
                            ],
                            [
                                'local_id' => 2,
                                'name' => '-- Voided --',
                                'short_name' => '-- VOID --',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'void',
                                'created_at' => '2019-01-23T22:29:49.743Z'
                            ],
                        ],
                        'account_id'       => 1,
                        'delivery_type_id' => 2,
                        'local_id'         => 197,
                        'status'           => 'void',
                        'currency'         => 'GBP',
                        'user_id'          => 1,
                        'created_at'       => '2019-01-23T22:05:15.430Z',
                    ]
                ],
                [
                    [
                        'account_id'       => 1,
                        'delivery_type_id' => 2,
                        'client_id'        => 197,
                        'parent_id'        => null,
                        'terminal_id'      => 1,
                        'user_id'          => 1,
                        'currency'         => 'GBP',
                        'status'           => 'void',
                        'created_at'       => new Carbon('2019-01-23T22:05:15.430Z'),
                    ]
                ],
                [
                    [
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => AccountAdjustment::class,
                            'name'           => 'Account Payment',
                            'short_name'     => 'Account Payment',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 98,
                            'line_amount'    => 98,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T22:05:26.558Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => VoidTransaction::class,
                            'name'           => '-- Voided --',
                            'short_name'     => '-- VOID --',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T22:29:49.743Z')
                        ],
                    ]
                ],
                [
                    []
                ],
                [
                    [
                        'id'              => 1,
                        'current_balance' => 0,
                    ]
                ],
                [
                    [
                        [
                            'id'              => 1,
                            'account_id'      => 1,
                            'amount'          => 0,
                            'description'     => 'POS Deposit',
                            'created_at'      => new Carbon('2019-01-23T22:05:26.558Z')
                        ]
                    ]
                ]
            ],
            'Payment on account' => [
                [
                    [
                        'lines' => [
                            [
                                'local_id' => 1,
                                'product_id' => 1,
                                'name' => 'Hound of the Baskervilles',
                                'short_name' => 'Hound Baskervilles',
                                'tax' => [
                                    'rate' => 'gb_vat_exempt',
                                    'label' => 'British VAT Exempt',
                                    'percent' => '0'
                                ],
                                'qty' => 1,
                                'single_amount' => '99.99',
                                'line_amount' => '99.99',
                                'line_tax' => '0',
                                'type' => 'product',
                                'created_at' => '2019-01-23T21:37:43.166Z'
                            ],
                            [
                                'local_id' => 2,
                                'name' => 'Total',
                                'short_name' => 'Total',
                                'qty' => 1,
                                'single_amount' => '99.99',
                                'line_amount' => '99.99',
                                'line_tax' => '0',
                                'type' => 'total',
                                'created_at' => '2019-01-23T21:37:48.044Z'
                            ],
                            [
                                'local_id' => 3,
                                'name' => 'VAT',
                                'short_name' => 'VAT',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'tax',
                                'created_at' => '2019-01-23T21:37:48.046Z'
                            ],
                            [
                                'local_id' => 4,
                                'payment_id' => 4,
                                'name' => 'On Account',
                                'short_name' => 'On Account',
                                'qty' => -1,
                                'single_amount' => '99.99',
                                'line_amount' => '-99.99',
                                'line_tax' => '0',
                                'type' => 'payment',
                                'created_at' => '2019-01-23T21:37:48.046Z'
                            ],
                            [
                                'local_id' => 5,
                                'name' => 'Change',
                                'short_name' => 'Change',
                                'qty' => 1,
                                'single_amount' => '0',
                                'line_amount' => '0',
                                'line_tax' => '0',
                                'type' => 'change',
                                'created_at' => '2019-01-23T21:37:48.048Z'
                            ]
                        ],
                        'account_id'       => 1,
                        'delivery_type_id' => 2,
                        'local_id'         => 196,
                        'status'           => 'complete',
                        'currency'         => 'GBP',
                        'user_id'          => 3,
                        'created_at'       => '2019-01-23T21:37:34.119Z',
                    ]
                ],
                [
                    [
                        'account_id'       => 1,
                        'delivery_type_id' => 2,
                        'client_id'        => 196,
                        'parent_id'        => null,
                        'terminal_id'      => 1,
                        'user_id'          => 3,
                        'currency'         => 'GBP',
                        'status'           => 'complete',
                        'created_at'       => new Carbon('2019-01-23T21:37:34.119Z'),
                    ]
                ],
                [
                    [
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => Product::class,
                            'name'           => 'Hound of the Baskervilles',
                            'short_name'     => 'Hound Baskervilles',
                            'tax_rate'       => 'gb_vat_exempt',
                            'qty'            => 1,
                            'single_amount'  => 99.99,
                            'line_amount'    => 99.99,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T21:37:43.166Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Total::class,
                            'name'           => 'Total',
                            'short_name'     => 'Total',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 99.99,
                            'line_amount'    => 99.99,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T21:37:48.044Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Tax::class,
                            'name'           => 'VAT',
                            'short_name'     => 'VAT',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T21:37:48.046Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => 1,
                            'object_type'    => Payment::class,
                            'name'           => 'On Account',
                            'short_name'     => 'On Account',
                            'tax_rate'       => null,
                            'qty'            => -1,
                            'single_amount'  => 99.99,
                            'line_amount'    => -99.99,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T21:37:48.046Z')
                        ],
                        [
                            'transaction_id' => 1,
                            'object_id'      => null,
                            'object_type'    => Change::class,
                            'name'           => 'Change',
                            'short_name'     => 'Change',
                            'tax_rate'       => null,
                            'qty'            => 1,
                            'single_amount'  => 0,
                            'line_amount'    => 0,
                            'line_tax'       => 0,
                            'created_at'     => new Carbon('2019-01-23T21:37:48.048Z')
                        ]
                    ]
                ],
                [
                    [
                        [
                            'id'              => 1,
                            'payment_type_id' => 4,
                            'amount'          => 99.99,
                            'account_id'      => 1
                        ]
                    ]
                ],
                [
                    [
                        'id'              => 1,
                        'current_balance' => -99.99
                    ]
                ],
                [
                    []
                ]
            ]
        ];
    }

    /**
     * @covers       ::boot
     * @dataProvider dataBootProcessTransaction
     * @param array $postData             Transaction records posted by terminal
     * @param array $expectedTransactions Expected DB transaction records
     * @param array $expectedLines        Expected DB transaction line records, grouped by transaction
     * @param array $expectedPayments     Expected DB payment records, grouped by transaction
     * @param array $expectedAccounts     Expected DB account records, grouped by transaction
     * @param array $expectedAdjustments  Expected DB account adjustment records, grouped by transaction
     */
    public function testBootProcessTransaction(
        array $postData,
        array $expectedTransactions,
        array $expectedLines,
        array $expectedPayments,
        array $expectedAccounts,
        array $expectedAdjustments
    )
    {
        $api = Terminal::first()->api_key;

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $api
        ])
        ->json('POST', '/api/v1/transactions/', ['transactions' => $postData]);

        $response->assertJson([
            'authenticated' => true,
            'success'       => true,
        ]);

        // Check expected records exist in DB
        foreach ($expectedTransactions as $expectedTransaction) {
            $this->assertDatabaseHas('oppin_pos_transactions', $expectedTransaction);
        }
        $this->assertEquals(Transaction::count(), count($expectedTransactions));
        $count = 0;
        foreach ($expectedLines as $expectedLinesTransaction) {
            foreach ($expectedLinesTransaction as $expectedLine) {
                $this->assertDatabaseHas('oppin_pos_transaction_lines', $expectedLine);
            }
            $count += count($expectedLinesTransaction);
        }
        $this->assertEquals(TransactionLine::count(), $count);
        $count = 0;
        foreach ($expectedPayments as $expectedPaymentTransaction) {
            foreach ($expectedPaymentTransaction as $expectedPayment) {
                $this->assertDatabaseHas('oppin_pos_payments', $expectedPayment);
            }
            $count += count($expectedPaymentTransaction);
        }
        $this->assertEquals(Payment::count(), $count);
        $count = 0;
        foreach ($expectedAccounts as $expectedAccount) {
            $this->assertDatabaseHas('oppin_poshospitality_accounts', $expectedAccount);
        }
        $count = 0;
        foreach ($expectedAdjustments as $expectedAdjustmentsTransaction) {
            foreach ($expectedAdjustmentsTransaction as $expectedAdjustment) {
                $this->assertDatabaseHas('oppin_poshospitality_account_adjustments', $expectedAdjustment);
            }
            $count += count($expectedAdjustmentsTransaction);
        }
        $this->assertEquals(AccountAdjustment::count(), $count);
    }
}
