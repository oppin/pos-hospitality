<?php namespace Oppin\POSHospitality\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDeliveryTypeProductTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('delivery_type_product');
        Schema::create('delivery_type_product', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('delivery_type_id')->unsigned()->index();
            $table->integer('product_id')->unsigned()->index();
            $table->primary(['delivery_type_id', 'product_id'], 'delivery_type_product');
        });
    }

    public function down()
    {
        Schema::dropIfExists('delivery_type_product');
    }
}
