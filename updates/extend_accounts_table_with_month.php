<?php namespace Oppin\POSHospitality\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class ExtendAccountsTableWithMonth extends Migration
{
    public function up()
    {

        Schema::table('oppin_poshospitality_accounts', function(Blueprint $table) {
            $table->enum('month', [
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December',
            ])->nullable()->index();
        });
    }

    public function down()
    {
        Schema::table('oppin_poshospitality_accounts', function(Blueprint $table) {
            $table->dropColumn('month');
        });
    }
}
