<?php namespace Oppin\POSHospitality\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDeliveryTypeLocationTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('delivery_type_location');
        Schema::create('delivery_type_location', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('delivery_type_id')->unsigned()->index();
            $table->integer('location_id')->unsigned()->index();
            $table->primary(['delivery_type_id', 'location_id'], 'delivery_type_location');
        });
    }

    public function down()
    {
        Schema::dropIfExists('delivery_type_location');
    }
}
