<?php namespace Oppin\POSHospitality\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class ExtendPaymentTypesTableWithAccountFlag extends Migration
{
    public function up()
    {

        Schema::table('oppin_pos_payment_types', function(Blueprint $table) {
            $table->boolean('on_account')->default(false)->after('show_other');
        });
    }

    public function down()
    {
        Schema::table('oppin_pos_payment_types', function(Blueprint $table) {
            $table->dropColumn('on_account');
        });
    }
}
