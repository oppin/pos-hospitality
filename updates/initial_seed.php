<?php namespace Oppin\POSHospitality\Updates;

use Seeder;
use Oppin\POSHospitality\Models\Account;
use Oppin\POSHospitality\Models\DeliveryType;
use Faker\Factory;
use Oppin\POS\Models\PaymentType;

class SeedPOSHospitality extends Seeder
{
    public function run()
    {
        $faker = Factory::create('en_GB');

        Account::create([
            'title'          => $faker->title,
            'first_name'     => $faker->firstName,
            'last_name'      => $faker->lastName,
            'address_1'      => $faker->streetAddress,
            'address_2'      => $faker->streetName,
            'town_city'      => $faker->city,
            'county'         => $faker->county,
            'postcode'       => $faker->postcode,
            'telephone'      => $faker->mobileNumber,
            'email'          => $faker->email,
            'location_id'    => 1,
            'credit_limit'   => 100,
        ]);

        DeliveryType::create([
            'name'       => 'Eat In',
            'short_name' => 'Eat In',
        ]);

        DeliveryType::create([
            'name'       => 'Takeaway',
            'short_name' => 'Takeaway',
        ]);

        DeliveryType::create([
            'name'                 => 'Delivery',
            'short_name'           => 'Delivery',
            'is_account_required'  => true,
            'is_account_permitted' => true,
        ]);

        $paymentType = new PaymentType([
            'name'       => 'On Account',
            'short_name' => 'On Account',
            'currency'   => 'GBP',
            'is_active'  => true,
            'cash'       => false,
            'show_other' => true,
            'on_account' => true,
        ]);
        $paymentType->forceSave();
    }
}
