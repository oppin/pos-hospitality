<?php namespace Oppin\POSHospitality\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAccountAdjustmentsTable extends Migration
{
    public function up()
    {
        Schema::create('oppin_poshospitality_account_adjustments', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('account_id')->unsigned()->index();
            $table->float('amount');
            $table->string('description');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oppin_poshospitality_account_adjustments');
    }
}
