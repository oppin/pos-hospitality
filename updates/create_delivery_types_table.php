<?php namespace Oppin\POSHospitality\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDeliveryTypesTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('oppin_poshospitality_delivery_types');
        Schema::create('oppin_poshospitality_delivery_types', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->index();
            $table->string('short_name', 20)->nullable()->index();
            $table->boolean('is_active')->default(1);
            $table->boolean('is_account_required')->default(0);
            $table->boolean('is_account_permitted')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oppin_poshospitality_delivery_types');
    }
}
