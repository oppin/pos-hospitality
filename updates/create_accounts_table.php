<?php namespace Oppin\POSHospitality\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAccountsTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('oppin_poshospitlity_accounts');
        Schema::create('oppin_poshospitality_accounts', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 10)->nullable();
            $table->string('first_name', 30)->index();
            $table->string('last_name', 30)->index();
            $table->string('address_1', 50)->nullable();
            $table->string('address_2', 50)->nullable();
            $table->string('town_city', 30)->nullable();
            $table->string('county', 30)->nullable();
            $table->string('postcode', 10)->nullable();
            $table->string('telephone', 15)->nullable();
            $table->string('email', 100)->nullable();
            $table->integer('location_id')->unsigned()->index();
            $table->boolean('is_active')->default(1)->index();
            $table->float('credit_limit')->unsigned()->nullable();
            $table->float('current_balance')->default(0);
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oppin_poshospitality_accounts');
    }
}
