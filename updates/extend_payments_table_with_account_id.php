<?php namespace Oppin\POSHospitality\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class ExtendPaymentsTableWithAccountId extends Migration
{
    public function up()
    {

        Schema::table('oppin_pos_payments', function(Blueprint $table) {
            $table->integer('account_id')->nullable()->index();
        });
    }

    public function down()
    {
        Schema::table('oppin_pos_payments', function(Blueprint $table) {
            $table->dropColumn('account_id');
        });
    }
}
