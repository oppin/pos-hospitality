<?php namespace Oppin\POSHospitality\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDeliveryTypePaymentTypeTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('delivery_type_payment_type');
        Schema::create('delivery_type_payment_type', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('delivery_type_id')->unsigned()->index();
            $table->integer('payment_type_id')->unsigned()->index();
            $table->primary(['delivery_type_id', 'payment_type_id'], 'delivery_type_payment_type');
        });
    }

    public function down()
    {
        Schema::dropIfExists('delivery_type_payment_type');
    }
}
