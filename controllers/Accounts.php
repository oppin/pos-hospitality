<?php namespace Oppin\POSHospitality\Controllers;

use ApplicationException;
use BackendMenu;
use DB;
use Flash;
use Lang;
use Validator;
use ValidationException;
use Backend\Classes\Controller;
use Oppin\POSHospitality\Models\AccountAdjustment;
use Oppin\POS\Models\Payment;
use Oppin\POSHospitality\Models\Account;
use Oppin\POS\Models\Transaction;
use Oppin\POS\Models\TransactionLine;
use Carbon\Carbon;
use Oppin\POS\Models\Product;

/**
 * Accounts Back-end Controller
 */
class Accounts extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Oppin.POSHospitality', 'poshospitality', 'accounts');
    }

    public function update($recordId, $context = null)
    {
        $this->makeLists();
        return $this->asExtension('FormController')->update($recordId, $context);
    }

    public function statements()
    {
        $accountIds = explode(',', get('accounts'));
        $from = Carbon::parse(get('from'))->startOfDay();
        $to = Carbon::parse(get('to'))->endOfDay();
        $accounts = Account::whereIn('id', $accountIds)->get();
        $activities = [];
        foreach ($accountIds as $accountId) {
            $activities[$accountId] = $this->getActivity($accountId, $from, $to, true);
        }
        $this->vars['from'] = $from;
        $this->vars['to'] = $to;
        $this->vars['accounts'] = $accounts;
        $this->vars['activities'] = $activities;
    }

    /**
     * Bulk delete records.
     * @return void
     */
    public function index_onApplyFunds()
    {
        /*
         * Validate checked identifiers
         */
        $checkedIds = post('checked');

        if (!$checkedIds || !is_array($checkedIds) || !count($checkedIds)) {
            Flash::error('No accounts were selected');
            return $this->listRefresh();
        }

        /**
         * Validate form
         */
        $validator = Validator::make(post(), [
            'amount' => 'required|numeric',
            'reason' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        /*
         * Establish the list definition
         */
        $definition = 'list';

        $listConfig = $this->listGetConfig($definition);

        /*
         * Create the model
         */
        $class = $listConfig->modelClass;
        $model = new $class;
        $model = $this->listExtendModel($model, $definition);

        /*
         * Create the query
         */
        $query = $model->newQuery();
        $this->listExtendQueryBefore($query, $definition);

        $query->whereIn($model->getKeyName(), $checkedIds);
        $this->listExtendQuery($query, $definition);

        /*
         * Update records
         */
        $records = $query->get();

        if ($records->count()) {
            foreach ($records as $account) {
                DB::transaction(function () use (&$account) {
                    $account->adjustments()->create([
                        'amount'      => post('amount'),
                        'description' => post('reason')
                    ]);
                    $account->current_balance += post('amount');
                    $account->save();
                });
            }

            Flash::success(Lang::get('Account balances have been adjusted'));
        }
        else {
            Flash::error(Lang::get('No accounts were selected to be adjusted'));
        }

        return $this->listRefresh($definition);
    }

    public function onUpdateBalance($recordId)
    {
        $account = $this->formFindModelObject($recordId);

        $validator = Validator::make(post(), [
            'new_balance'        => 'required|numeric',
            'new_balance_reason' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        DB::transaction(function () use (&$account) {
            $difference = post('new_balance') - $account->current_balance;
            $account->adjustments()->create([
                'amount'      => $difference,
                'description' => post('new_balance_reason')
            ]);
            $account->current_balance = post('new_balance');
            $account->save();
        });

        $this->initForm($account);
        $fieldObject = $this->widget->form->getField('current_balance');
        $result['#' . $fieldObject->getId('group')] = $this->widget->form->makePartial('field', ['field' => $fieldObject]);
        return $result;
    }

    public function onLoadActivity($recordId): array
    {
        $startDate = Carbon::parse(post('options.dates.0', 'first day of this month'));
        $endDate = Carbon::parse(post('options.dates.1', 'now'));

        $activity = $this->getActivity($recordId, $startDate, $endDate);

        return [
            '#ledger-tbody' => $this->makePartial('ledger_rows'),
            '#ledger-in-total' => number_format($activity['inTotal'], 2),
            '#ledger-out-total' => number_format($activity['outTotal'], 2),
            '#products-tbody' => $this->makePartial('products_rows')
        ];
    }

    protected function getActivity($recordId, Carbon $from, Carbon $to, bool $withTransactions = false): array
    {
        $ledgerLines = TransactionLine::select('created_at', 'name', 'line_amount', 'transaction_id')
            ->whereHas('transaction', function($query) use ($recordId) {
                $query->where('status', 'complete')
                    ->where('account_id', $recordId);
            })
            ->whereDate('created_at', '>=', $from)
            ->whereDate('created_at', '<=', $to)
            ->where(function($query) use ($recordId) {
                $query->where(function($query) use ($recordId) {
                    $query->where('object_type', Payment::class)
                        ->whereIn(
                            'object_id',
                            Payment::whereHas('payment_type', function($query) {
                                    $query->where('on_account', true);
                                })
                                ->where('account_id', $recordId)
                                ->lists('id')
                        );
                });
            });
        $ledgerQuery = AccountAdjustment::select('oppin_poshospitality_account_adjustments.created_at', 'description AS name', 'amount AS line_amount', 'transaction_id')
            ->leftjoin('oppin_pos_transaction_lines', function($join) {
                $join->on('oppin_poshospitality_account_adjustments.id', '=', 'oppin_pos_transaction_lines.object_id')
                    ->where('oppin_pos_transaction_lines.object_type', AccountAdjustment::class);
            })
            ->where('account_id', $recordId)
            ->whereDate('oppin_poshospitality_account_adjustments.created_at', '>=', $from)
            ->whereDate('oppin_poshospitality_account_adjustments.created_at', '<=', $to)
            ->unionAll($ledgerLines)
            ->orderBy('created_at', 'desc');
        if ($withTransactions) {
            $ledgerQuery->with('transaction.lines');
        }
        $ledger = $ledgerQuery->get();

        // Loop through ledger and add a running total to each record
        // TODO - If end date is before today, the total will be wrong as it is calulated from today back
        $balance = Account::where('id', $recordId)->value('current_balance');

        // Adjust balance back to statement end date
        if (!$to->isToday()) {
            $paymentsSum = TransactionLine::whereHas('transaction', function($query) use ($recordId) {
                    $query->where('status', 'complete')
                        ->where('account_id', $recordId);
                })
                ->whereDate('created_at', '>', $to)
                ->whereDate('created_at', '<=', Carbon::now())
                ->where(function($query) use ($recordId) {
                    $query->where(function($query) use ($recordId) {
                        $query->where('object_type', Payment::class)
                            ->whereIn(
                                'object_id',
                                Payment::whereHas('payment_type', function($query) {
                                        $query->where('on_account', true);
                                    })
                                    ->where('account_id', $recordId)
                                    ->lists('id')
                            );
                    });
                })
                ->sum('line_amount');
            $adjustmentsSum = AccountAdjustment::leftjoin('oppin_pos_transaction_lines', function($join) {
                    $join->on('oppin_poshospitality_account_adjustments.id', '=', 'oppin_pos_transaction_lines.object_id')
                        ->where('oppin_pos_transaction_lines.object_type', AccountAdjustment::class);
                })
                ->where('account_id', $recordId)
                ->whereDate('oppin_poshospitality_account_adjustments.created_at', '>', $to)
                ->whereDate('oppin_poshospitality_account_adjustments.created_at', '<=', Carbon::now())
                ->sum('amount');
            $balance -= $paymentsSum + $adjustmentsSum;
        }

        $adjustment = 0;
        $ledger->transform(function($line) use (&$balance, &$adjustment) {
            $balance = $line->balance = $balance - $adjustment;
            $adjustment = $line->line_amount;
            return $line;
        });

        $products = TransactionLine::select(DB::raw('MIN(name) as name'), DB::raw('COUNT(*) as count'), DB::raw('SUM(line_amount) as total'))
            ->whereHas('transaction', function($query) use ($recordId) {
                $query->where('status', 'complete')
                    ->where('account_id', $recordId);
            })
            ->whereDate('created_at', '>=', $from)
            ->whereDate('created_at', '<=', $to)
            ->where('object_type', Product::class)
            ->groupBy('object_id')
            ->orderBy('total', 'desc')
            ->get();

        $this->vars['lines'] = $ledger;
        $this->vars['products'] = $products;
        $inTotal = $ledger->filter(function($line) {
            return $line->line_amount > 0;
        })->sum('line_amount');
        $outTotal = abs($ledger->filter(function($line) {
            return $line->line_amount < 0;
        })->sum('line_amount'));

        return compact('inTotal', 'outTotal', 'ledger', 'products');
    }
}
