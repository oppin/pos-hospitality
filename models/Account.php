<?php namespace Oppin\POSHospitality\Models;

use Model;
use Oppin\POS\Models\Barcode;

/**
 * Account Model
 */
class Account extends Model
{
    use \October\Rain\Database\Traits\Nullable;
    use \October\Rain\Database\Traits\Revisionable;
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_poshospitality_accounts';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Fields to convert to Carbon objects
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array Monitor these attributes for changes.
     */
    protected $revisionable = [
        'title',
        'first_name',
        'last_name',
        'address_1',
        'address_2',
        'town_city',
        'county',
        'postcode',
        'telephone',
        'email',
        'location_id',
        'is_active',
        'credit_limit',
        'notes',
        'month',
    ];

    /**
     * @var array Nullable attributes.
     */
    protected $nullable = [
        'address_1',
        'address_2',
        'town_city',
        'county',
        'postcode',
        'telephone',
        'email',
        'notes',
        'month',
    ];

    /**
     * @var array The attributes to hide from the model's array form.
     */
    protected $hidden = [];

    /**
     * @var array The accessors to append to the model's array form.
     */
    protected $appends = [];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'title'                   => 'max:10',
        'first_name'              => 'required|max:30',
        'last_name'               => 'required|max:30',
        'address_1'               => 'nullable|required_with:address_2,town_city,postcode|max:50',
        'address_2'               => 'nullable|max:50',
        'town_city'               => 'nullable|required_with:address_1,postcode|max:30',
        'county'                  => 'nullable|max:30',
        'postcode'                => 'nullable|max:10',
        'telephone'               => 'nullable|max:15',
        'email'                   => 'nullable|email|max:100',
        'is_active'               => 'boolean',
        'credit_limit'            => 'required|numeric|min:0',
        'current_balance'         => 'numeric',
        'month'                   => 'nullable|in:January,February,March,April,May,June,July,August,September,October,November,December',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'adjustments' => 'Oppin\POSHospitality\Models\AccountAdjustment',
        'payments'    => 'Oppin\POS\Models\Payment',
    ];
    public $belongsTo = [
        'location' => 'Oppin\POS\Models\Location',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [
        'barcodes' => [
            'Oppin\POS\Models\Barcode',
            'name' => 'object'
        ],
        'revision_history' => [
            'System\Models\Revision',
            'name' => 'revisionable'
        ]
    ];
    public $attachOne = [];
    public $attachMany = [];

    //
    // Mutators
    //

    public function getBarcodesTextAttribute()
    {
        return $this->barcodes->implode('barcode', "\n");
    }

    public function setBarcodesTextAttribute($value)
    {
        $barcodes = collect(explode("\n", str_replace("\r\n", "\n", $value)));
        $barcodes = $barcodes->map(function ($barcode) {
            return trim($barcode);
        })->filter(function($barcode) {
            return !empty($barcode);
        });

        // Check if barcode exists on this account
        $existing = collect($this->barcodes()->withDeferred(post('_session_key'))->lists('barcode'));

        // Find which barcodes need creating or deleting
        $new = $barcodes->diff($existing);
        $removed = $existing->diff($barcodes);

        // Create new barcodes
        foreach ($new as $barcode) {
            $model = Barcode::create([
                'barcode' => $barcode
            ]);
            $this->barcodes()->add($model, post('_session_key'));
        }

        // Delete old barcodes
        foreach ($removed as $barcode) {
            $model = $this->barcodes()
                ->withDeferred(post('_session_key'))
                ->where('barcode', $barcode)
                ->first();
            $this->barcodes()->delete($model, post('_session_key'));
        }
    }

    public function getCreditLimitAttribute($value)
    {
        return (float)$value;
    }

    //
    // Scopes
    //

    public function scopeActive($query)
    {
        $query->where('is_active', true);
    }
}
