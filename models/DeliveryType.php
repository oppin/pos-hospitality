<?php namespace Oppin\POSHospitality\Models;

use Model;

/**
 * DeliveryType Model
 */
class DeliveryType extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_poshospitality_delivery_types';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Fields to convert to Carbon objects
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array The attributes to hide from the model's array form.
     */
    protected $hidden = [];

    /**
     * @var array The accessors to append to the model's array form.
     */
    protected $appends = [];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'                    => 'required|max:255',
        'short_name'              => 'required|max:20',
        'is_active'               => 'boolean',
        'is_account_required'     => 'boolean',
        'is_account_permitted'    => 'boolean',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'locations'     => 'Oppin\POS\Models\Location',
        'payment_types' => 'Oppin\POS\Models\PaymentType',
        'products'      => 'Oppin\POS\Models\Product',
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    //
    // Scopes
    //

    public function scopeActive($query)
    {
        $query->where('is_active', true);
    }
}
