<?php namespace Oppin\POSHospitality\Models;

use Model;

/**
 * AccountAdjustment Model
 */
class AccountAdjustment extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'oppin_poshospitality_account_adjustments';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['account_id', 'amount', 'description', 'created_at'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'account' => 'Oppin\POSHospitality\Models\Account',
        'transaction' => 'Oppin\POS\Models\Transaction',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [
        'transaction_line' => [
            'Oppin\POS\Models\TransactionLine',
            'name' => 'object'
        ]
    ];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
