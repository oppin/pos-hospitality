<?php namespace Oppin\POSHospitality;

use Backend;
use Event;
use System\Classes\PluginBase;
use Oppin\POS\Models\Location;
use Oppin\POS\Models\PaymentType;
use Oppin\POS\Models\Product;
use Oppin\POS\Models\Payment;
use Oppin\POS\Controllers\Locations;
use Oppin\POS\Models\Settings;
use Oppin\POSHospitality\Models\DeliveryType;
use System\Controllers\Settings as SettingsController;
use Carbon\Carbon;
use Oppin\POS\Controllers\Products;
use Oppin\POS\Models\Barcode;
use Oppin\POSHospitality\Models\Account;
use Oppin\POSHospitality\Models\AccountAdjustment;
use Oppin\POS\Models\Transaction;

/**
 * POSHospitality Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * @var array Plugin dependencies
     */
    public $require = ['Oppin.POS'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'POS Hospitality',
            'description' => 'A hospitality extension to Oppin POS',
            'author'      => 'Oppin',
            'icon'        => 'icon-calculator'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        Barcode::extend(function($model) {
            // Class map
            $model->typeMap['Oppin\POSHospitality\Models\Account'] = 'account';
        });

        Location::extend(function($model) {
            // Validation
            $model->rules['delivery_types'] = 'required';

            // Relations
            $model->hasMany['accounts'] = 'Oppin\POSHospitality\Models\Account';
            $model->belongsToMany['delivery_types'] = 'Oppin\POSHospitality\models\DeliveryType';
        });

        Payment::extend(function($model) {
            $model->belongsTo['account'] = 'Oppin\POSHospitality\Models\Account';
        });

        PaymentType::extend(function($model) {
            $model->belongsToMany['delivery_types'] = 'Oppin\POSHospitality\models\DeliveryType';

            $model->addDynamicMethod('getDeliveryTypeIdsAttribute', function() use ($model) {
                return $model->delivery_types->pluck('id');
            });

            $model->addHidden('delivery_types');
            $model->setAppends(array_merge($model->appends ?: [], ['delivery_type_ids']));
        });

        Product::extend(function($model) {
            // Validation
            $model->rules['delivery_types'] = 'required_unless:type,variations';

            // Relation
            $model->belongsToMany['delivery_types'] = 'Oppin\POSHospitality\Models\DeliveryType';

            $model->addDynamicMethod('getDeliveryTypeIdsAttribute', function() use ($model) {
                return $model->delivery_types->pluck('id');
            });

            $model->addHidden('delivery_types');
            $model->setAppends(array_merge($model->appends ?: [], ['delivery_type_ids']));
        });

        Settings::extend(function($model) {
            $model->addDynamicMethod('getDeliveryTypesAttribute', function() {
                return DeliveryType::all()->toArray();
            });

            $model->addDynamicMethod('setDeliveryTypesAttribute', function($value) {
                DeliveryType::whereNotIn('id', collect($value)->pluck('id'))
                    ->update([
                        'deleted_at' => Carbon::now(),
                    ]);
                foreach ($value as $delivery_type) {
                    if (!empty($delivery_type['id'])) {
                        $record = DeliveryType::find($delivery_type['id']);
                    } else {
                        $record = new DeliveryType;
                    }
                    $record->name                 = $delivery_type['name'];
                    $record->short_name           = $delivery_type['short_name'];
                    $record->is_active            = !empty($delivery_type['is_active']);
                    $record->is_account_required  = !empty($delivery_type['is_account_required']);
                    $record->is_account_permitted = !empty($delivery_type['is_account_permitted']);
                    $record->save();
                }
            });

            $model->addDynamicMethod('getDeliveryTypeIdsOptions', function() {
                return DeliveryType::lists('name', 'id');
            });
        });

        Transaction::extend(function($model) {
            $model->belongsTo['delivery_type'] = 'Oppin\POSHospitality\Models\DeliveryType';
            $model->belongsTo['account'] = 'Oppin\POSHospitality\Models\Account';
        });

        Locations::extendFormFields(function($form, $model) {
            if (!$model instanceof Location || $form->isNested) {
                return;
            }

            // Add button type
            $buttonsField = $form->getField('buttons');
            $config = $buttonsField->config;
            $config['groups']['account_payment'] = [
                'name'        => 'Account Payment',
                'description' => 'Allows payment into an account',
                'fields'      => [
                    'title' => [
                        'label'       => 'Account Payment',
                        'disabled'    => true,
                        'attributes'  => [
                            'style' => 'display:none;',
                        ],
                        'containerAttributes' => [
                            'style' => 'padding-bottom:4px;',
                        ],
                    ],
                ],
            ];
            $buttonsField->config = $config;

            $form->addFields([
                'delivery_types' => [
                    'label'        => 'Delivery Types',
                    'commentAbove' => 'Select types available at this location',
                    'type'         => 'relation',
                    'nameFrom'     => 'name',
                    'span'         => 'left',
                ],
            ]);
        });

        Products::extendFormFields(function($form, $model) {
            if (!$model instanceof Product || $form->isNested) {
                return;
            }

            $form->addTabFields([
                'delivery_types' => [
                    'tab'          => 'Details',
                    'label'        => 'Delivery Types',
                    'commentAbove' => 'Select types for which this product is available',
                    'type'         => 'relation',
                    'nameFrom'     => 'name',
                    'span'         => 'left',
                    'trigger'      => [
                        'action'    => 'hide',
                        'field'     => 'type',
                        'condition' => 'value[variations]'
                    ],
                ]
            ]);
        });

        // Extend settings form
        Event::listen('backend.form.extendFields', function ($form) {
            if (!$form->model instanceof Settings) {
                return;
            }

            if ($form->isNested) {
                if (!starts_with($form->alias, 'formPaymentTypesForm')) {
                    return;
                }

                $form->addFields([
                    'delivery_type_ids' => [
                        'label'        => 'Delivery Types',
                        'commentAbove' => 'Select types for which this payment is available',
                        'type'         => 'checkboxlist',
                        'span'         => 'right',
                    ],
                    'on_account' => [
                        'label'        => 'On Account',
                        'comment'      => 'The balance will be deducted from the selected account',
                        'type'         => 'checkbox',
                        'span'         => 'left',
                        'trigger'      => [
                            'action'     => 'show',
                            'field'      => 'cash',
                            'condition'  => 'unchecked',
                        ],
                    ]
                ]);
                return;
            }

            $form->addTabFields([
                'delivery_types' => [
                    'tab'    => 'Delivery Types',
                    'label'  => 'Delivery Types',
                    'type'   => 'repeater',
                    'prompt' => 'Add new delivery type',
                    'form'   => [
                        'fields' => [
                            'id' => [
                                'type' => 'partial',
                                'path' => '$/oppin/pos/controllers/settings/_id.htm',
                            ],
                            'name' => [
                                'label'    => 'Name',
                                'span'     => 'left',
                                'required' => true,
                            ],
                            'short_name' => [
                                'label'    => 'Short Name',
                                'span'     => 'right',
                                'required' => true,
                            ],
                            'is_account_permitted' => [
                                'label'    => 'Account Permitted',
                                'comment'  => 'An account is permitted to be choosen with this delivery type',
                                'type'     => 'checkbox',
                                'span'     => 'left',
                            ],
                            'is_account_required' => [
                                'label'    => 'Account Required',
                                'comment'  => 'An account is required to choose this delivery type',
                                'type'     => 'checkbox',
                                'span'     => 'right',
                                'trigger'  => [
                                    'action'    => 'show',
                                    'field'     => 'is_account_permitted',
                                    'condition' => 'checked'
                                ],
                            ],
                            'is_active'  => [
                                'label'    => 'Active',
                                'comment'  => 'Is this delivery type available to choose?',
                                'type'     => 'switch',
                                'default'  => 1,
                                'span'     => 'full',
                            ],

                        ]
                    ]
                ]
            ]);
        });

        Event::listen('oppin.pos.settings.paymentTypes.beforeSave', function(&$model, $input) {
            $delivery_types = is_array($input['delivery_type_ids']) ? $input['delivery_type_ids'] : [];
            $delivery_types = array_map('intval', $delivery_types);
            $model->delivery_types = $delivery_types;
            $model->on_account = empty($input['cash']) && !empty($input['on_account']);
        });

        // Extend terminal initialisation API result
        Event::listen('oppin.pos.api.v1.terminal.update', function(&$result, $terminal) {
            $result['delivery_types'] = $terminal->location->delivery_types()
                ->active()
                ->get()
                ->keyBy('id');

            $result['accounts'] = $terminal->location->accounts()
                ->active()
                ->orderBy('last_name', 'asc')
                ->orderBy('first_name', 'asc')
                ->get()
                ->keyBy('id');

            // Update buttons
            $products = $result['products'];
            $result['buttons'] = collect($result['buttons'])
                ->map(function($button) use ($products, $result) {
                    if ($button['type'] == 'product') {
                        // Add delivery types applicable to products
                        if (!empty($button['buttons'])) {
                            $allDeliveryTypeIds = collect();
                            foreach($button['buttons'] as &$childButton) {
                                $childButton['delivery_type_ids'] = $products->get($childButton['product_id'])->delivery_type_ids;
                                $allDeliveryTypeIds = $allDeliveryTypeIds->merge($childButton['delivery_type_ids']);
                            }
                            $button['delivery_type_ids'] = $allDeliveryTypeIds->unique()->values();
                        } else {
                            $button['delivery_type_ids'] = $products->get($button['product_id'])->delivery_type_ids;
                        }
                    } elseif ($button['label'] == 'account_payment') {
                        // Relabel account payment button
                        $button['label'] = 'Account Payment';
                        $button['delivery_type_ids'] = collect($result['delivery_types'])
                            ->where('is_account_permitted', true)
                            ->pluck('id');
                    }
                    return $button;
                })
                ->all();
        });

        Event::listen('oppin.pos.api.v1.transactions.process', function($requestTransaction, &$transaction) {
            // Extend transaction account storage
            if (!empty($requestTransaction['account_id'])) {
                $transaction->account_id = $requestTransaction['account_id'];
            }
            if (!empty($requestTransaction['delivery_type_id'])) {
                $transaction->delivery_type_id = $requestTransaction['delivery_type_id'];
            }
        });

        Event::listen('oppin.pos.api.v1.transactions.processLine', function($requestTransaction, $requestLine, &$line) {
            // Extend transaction payment storage
            if (array_key_exists('payment_id', $requestLine)) {
                if (!empty($requestTransaction['account_id']) && $line->object && $line->object->payment_type->on_account) {
                    $payment = $line->object;
                    $payment->account_id = $requestTransaction['account_id'];
                    $payment->save();

                    if (!$line->exists) {
                        Account::where('id', $requestTransaction['account_id'])
                            // Line amount will be negative (unless this is a refund), so we use increment to deduct
                            ->increment('current_balance', $line->line_amount);
                    }
                }
            }

            // Extend transaction storage for payment of accounts
            if ($requestLine['type'] == 'account_payment' && !$line->exists) {
                $amount = $requestTransaction['status'] == 'complete' ? $line->line_amount : 0;
                $adjustment = AccountAdjustment::create([
                    'account_id'  => $requestTransaction['account_id'],
                    'amount'      => $amount,
                    'description' => 'POS Deposit',
                    'created_at'  => $line->created_at
                ]);
                Account::where('id', $requestTransaction['account_id'])
                    ->increment('current_balance', $amount);
                $line->object_id = $adjustment->id;
                $line->object_type = AccountAdjustment::class;
            }
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Oppin\POSHospitality\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'oppin.pos.accounts.manage' => [
                'tab' => 'POS Accounts',
                'label' => 'Manage Customer Accounts'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'accounts' => [
                'label'       => 'Accounts',
                'url'         => Backend::url('oppin/poshospitality/accounts'),
                'icon'        => 'icon-address-book',
                'permissions' => ['oppin.pos.accounts.*'],
                'order'       => 450,
            ],
        ];
    }
}
